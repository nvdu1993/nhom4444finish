const { I } = inject();

module.exports = {
  updateAlert: {
    laterButton: '//android.widget.Button[@resource-id=\"android:id/button1\"]',
  },
  user:{
    password: '//android.widget.EditText[@content-desc=\"login_password\"]',
    signIn:'//android.widget.EditText[@content-desc="login_signIn"]',
  },
  login: {
    skipBtn: '//android.view.ViewGroup[@content-desc="tutorial_skip"]',
    // activationCode0: '//android.widget.EditText[@content-desc="activation_0"]',
 
    baseXpathActivationCode :  '//android.widget.EditText[@content-desc=\"activation_',
    getXpathCode(index) {
      return this.baseXpathActivationCode + String(index) + '\"]';
    },
    password: '//android.widget.EditText[@content-desc="login_password"]',
    signIn:'//android.widget.EditText[@content-desc="login_signIn"]',
   
    nextBtn: '//android.view.ViewGroup[@content-desc="next"]',
                        //android.view.ViewGroup[@content-desc="next"]/android.widget.TextView

    signInBtn: '//android.view.ViewGroup[@content-desc="login_signIn"]',

    // OTP0: '//android.widget.EditText[@content-desc="otp_0"]',
    // OTP1: '//android.widget.EditText[@content-desc="otp_1"]',
    // OTP2: '//android.widget.EditText[@content-desc="otp_2"]',
    // OTP3: '//android.widget.EditText[@content-desc="otp_3"]',
    // OTP4: '//android.widget.EditText[@content-desc="otp_4"]',
    // OTP5: '//android.widget.EditText[@content-desc="otp_5"]',
    baseXpathOtpCode : '//android.widget.EditText[@content-desc=\"otp_', 
   
    getXpathOtpCode(index) {
      return this.baseXpathOtpCode + String(index) + '\"]';
    },
    confirm: '//android.view.ViewGroup[@content-desc="confirm"]',
    btnym: '//android.view.ViewGroup[@content-desc="bottomTab_settings"]',
    btnsitting: '//android.view.ViewGroup[@content-desc="account.settings"]',
    btnlanguage: '//android.view.ViewGroup[@content-desc="settings_language"]/android.widget.TextView',
    btnjapanese:  '//android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]',
    btnconfirm: '//android.view.ViewGroup[@content-desc="confirm"]',
    btnback: '//android.view.ViewGroup[@content-desc="ic_back"]/android.widget.TextView', 
    btncontact:	'//android.view.ViewGroup[@content-desc=\"bottomTab_contact\"]',
    btnclient: '//android.view.ViewGroup[@content-desc="contact_tab_clients"]',
    btnteam: '//android.view.ViewGroup[@content-desc="contact_tab_teams"]',
    btncontactsearch : '//android.widget.EditText[@content-desc="contact_search"]',
    btnuser1: '//android.view.ViewGroup[@content-desc="clients_20_contact_0"]'
  }
 
  // insert your locators and methods here
}