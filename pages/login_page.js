const { I } = inject();

module.exports = {

  login: {
    userName : "//input[@id='user[login]']",
    emailPath :  "//input[@id='user[email]']",
    passWord : "//input[@id='user[password]']",
    signUpButton: "(//button[text()='Sign up for GitHub'])[1]"
  }

}
